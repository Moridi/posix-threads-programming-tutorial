/******************************************************************************
* FILE: 1-creation.c
* DESCRIPTION:
*   A "hello world" Pthreads program.  Demonstrates thread creation and
*   termination.
* AUTHOR: Blaise Barney
* LAST REVISED: 2020-04-16 Mohammad Moridi
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include <pthread.h>

#define NUMBER_OF_THREADS 4

void* print_hello(void* args)
{
	printf("Hello World! It's me. Do you know me?!\n");
	pthread_exit(NULL);
}

int main()
{
	pthread_t threads[NUMBER_OF_THREADS];
	int return_code;

	for(long tid = 0; tid < NUMBER_OF_THREADS; tid++)
	{
		printf("In main: creating thread %ld\n", tid);
		return_code = pthread_create(&threads[tid],
				NULL, print_hello, NULL);

		if (return_code)
		{
			printf("ERROR; return code from pthread_create() is %d\n",
					return_code);
			exit(-1);
		}
	}

	/* Last thing that main() should do */
	pthread_exit(NULL);
}