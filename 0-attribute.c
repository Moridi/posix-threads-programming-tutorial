/******************************************************************************
* FILE: 0-attribute.c
* DESCRIPTION:
*   Demonstrates thread creation and working with attributes.
* AUTHOR: Mohammad Moridi
* LAST REVISED: 2020-04-16 Mohammad Moridi
******************************************************************************/

#include <stdio.h>
// #include <unistd.h>
#include <limits.h>

#include <pthread.h>

void* do_nothing(void* args)
{
    // sleep(1);
    // printf("Hello world!\n");
}

int main()
{
    pthread_t tid;
    pthread_attr_t attr;
    size_t stack_size;

    pthread_attr_init (&attr);
    pthread_attr_setstacksize(&attr, PTHREAD_STACK_MIN + 1000);
    pthread_attr_getstacksize(&attr, &stack_size);
    printf("Stack Size: %ld\n", stack_size);

    pthread_create (&tid, &attr, do_nothing, NULL);
    pthread_attr_destroy (&attr);

    pthread_exit(NULL);
}