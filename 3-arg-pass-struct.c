/******************************************************************************
* FILE: 3-arg-pass-struct.c
* DESCRIPTION:
*   A "hello world" Pthreads program which demonstrates another safe way
*   to pass arguments to threads during thread creation.  In this case,
*   a structure is used to pass multiple arguments.
* AUTHOR: Blaise Barney
* LAST REVISED: 2020-04-16 Mohammad Moridi
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include <pthread.h>

#define NUMBER_OF_THREADS 4
#define RESET_COLOR_INDEX 4

char COLOR_CODES[5][255] =
{
	"\033[1m\033[30m",		// Bold BLACK
	"\033[1m\033[31m",		// Bold RED
	"\033[1m\033[32m",		// Bold GREEN
	"\033[1m\033[34m",		// Bold BLUE
	"\x1B[0m"				// Reset
};

struct thread_data
{
   int thread_id;
   char* message;
};
struct thread_data thread_data_array[NUMBER_OF_THREADS];

void* print_hello(void* data)
{
	struct thread_data* my_data = (struct thread_data*) data;
	int thread_id = my_data->thread_id;
	char* thread_message = my_data->message;

	char result[255];
	sprintf(result, "%s%s\nIt's me, thread #%d!%s\n",
			COLOR_CODES[thread_id],
			thread_message,
			thread_id,
			COLOR_CODES[RESET_COLOR_INDEX]);
	printf("%s", result);

	pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
	pthread_t threads[NUMBER_OF_THREADS];
	int* thread_ids[NUMBER_OF_THREADS];
	int return_code;

	char messages[NUMBER_OF_THREADS][255] =
	{
		"English: Hello World!",
		"French: Bonjour, le monde!",
		"Spanish: Hola al mundo",
		"Klingon: Nuq neH!"
	};

	for(long tid = 0; tid < NUMBER_OF_THREADS; tid++)
	{
		thread_data_array[tid].thread_id = tid;
		thread_data_array[tid].message = messages[tid];

		printf("Creating thread %ld\n", tid);

		return_code = pthread_create(&threads[tid], NULL, print_hello,
				(void*)&thread_data_array[tid]);

		if (return_code)
		{
			printf("ERROR; return code from pthread_create() is %d\n",
					return_code);
			exit(-1);
		}
	}
	pthread_exit(NULL);
}