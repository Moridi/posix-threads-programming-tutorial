/*****************************************************************************
* FILE: 7-dot_prod_mutex.c
* DESCRIPTION:
*   This example program illustrates the use of mutex variables 
*   in a threads program. This version was obtained by modifying the
*   serial version of the program (dot_prod_serialuct.c) which performs a 
*   dot product. The main data is made available to all threads through 
*   a globally accessible  structure. Each thread works on a different 
*   part of the data. The main thread waits for all the threads to complete 
*   their computations, and then it prints the resulting sum.
* SOURCE: Vijay Sonnad, IBM
* LAST REVISED: 2020-04-16 Mohammad Moridi
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include <pthread.h>

/*   
The following structure contains the necessary information  
to allow the function "dot_product" to access its input data and 
place its output into the structure.  This structure is 
unchanged from the sequential version.
*/

typedef struct 
{
	double* a;
	double* b;
	double sum; 
	int length; 
} DOT_DATA;

/* Define globally accessible variables and a mutex */

#define NUMBER_OF_THREADS 4
#define LENGTH 1e7
DOT_DATA dot_product_result; 
pthread_t threads[NUMBER_OF_THREADS];
pthread_mutex_t mutex_sum;

/*
The function dot_product is activated when the thread is created.
As before, all input to this routine is obtained from a structure 
of type DOT_DATA and all output from this function is written into
this structure. The benefit of this approach is apparent for the 
multi-threaded program: when a thread is created we pass a single
argument to the activated function - typically this argument
is a thread number. All  the other information required by the 
function is accessed from the globally accessible structure. 
*/

#define RESET_COLOR_INDEX 4

char COLOR_CODES[5][255] =
{
	"\033[1m\033[30m",		// Bold BLACK
	"\033[1m\033[31m",		// Bold RED
	"\033[1m\033[32m",		// Bold GREEN
	"\033[1m\033[34m",		// Bold BLUE
	"\x1B[0m"				// Reset
};

void* dot_product(void* arg)
{
	/* Define and use local variables for convenience */

	long offset = (long)arg;
	int len = dot_product_result.length / NUMBER_OF_THREADS;
	
	int start = offset * len;
	int end = start + len;
	
	double* x = dot_product_result.a;
	double* y = dot_product_result.b;
	
	double my_sum = 0;
	char result[255];

	/*
	Perform the dot product and assign result
	to the appropriate variable in the structure. 
	*/
	for (int i = start; i < end ; i++)
    { 
    	pthread_mutex_lock (&mutex_sum);
		dot_product_result.sum += (x[i] * y[i]);
    	pthread_mutex_unlock (&mutex_sum);
    }

	pthread_exit((void*)0);
}

/* 
The main program creates threads which do all the work and then 
print out result upon completion. Before creating the threads,
The input data is created. Since all threads update a shared structure, we
need a mutex for mutual exclusion. The main thread needs to wait for
all threads to complete, it waits for each one of the threads. We specify
a thread attribute value that allow the main thread to join with the
threads it creates. Note also that we free up handles  when they are
no longer needed.
*/

int main (int argc, char *argv[])
{
	void *status;

	/* Assign storage and initialize values */

	double* a = (double*)malloc (NUMBER_OF_THREADS *
			LENGTH * sizeof(double));
	double* b = (double*)malloc (NUMBER_OF_THREADS *
			LENGTH * sizeof(double));

	for(long i = 0; i < LENGTH * NUMBER_OF_THREADS; i++)
		b[i] = a[i] = 1;

	dot_product_result.length = LENGTH;
	dot_product_result.a = a; 
	dot_product_result.b = b; 
	dot_product_result.sum = 0;

	pthread_mutex_init(&mutex_sum, NULL);

	for(long i = 0; i < NUMBER_OF_THREADS; i++)
		/* Each thread works on a different set of data.
		* The offset is specified by 'i'. The size of
		* the data for each thread is indicated by LENGTH.
		*/
		pthread_create(&threads[i], NULL, dot_product, (void*)i); 

	/* Wait on the other threads */
	for(long i = 0; i < NUMBER_OF_THREADS; i++)
		pthread_join(threads[i], &status);
	
	/* After joining, print out the results and cleanup */
	printf("Sum =  %f \n", dot_product_result.sum);
	free(a);
	free(b);
	pthread_mutex_destroy(&mutex_sum);
	pthread_exit(NULL);
}