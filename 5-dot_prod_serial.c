/******************************************************************************
* FILE: 5-dot_product_serial.c
* DESCRIPTION:
*   This is a simple serial program which computes the dot product of two 
*   vectors.  The threaded version can is dot_product_mutex.c.
* SOURCE: Vijay Sonnad, IBM
* LAST REVISED: 2020-04-16 Mohammad Moridi
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

/*   
The following structure contains the necessary information  
to allow the function "dot_product" to access its input data and 
place its output so that it can be accessed later. 
*/

typedef struct 
{
	double* a;
	double* b;
	double sum; 
	int length; 
} DOT_DATA;

#define LENGTH 1e7
DOT_DATA dot_product_result; 

/*
We will use a function (dot_product) to perform the scalar product. 
All input to this routine is obtained through a structure of 
type DOT_DATA and all output from this function is written into
this same structure.  While this is unnecessarily restrictive 
for a sequential program, it will turn out to be useful when
we modify the program to compute in parallel.
*/

void dot_product()
{
	/* Define and use local variables for convenience */

	int start = 0;
	int end = dot_product_result.length; 
	double my_sum = 0;
	double* x = dot_product_result.a;
	double* y = dot_product_result.b;

	/*
	Perform the dot product and assign result
	to the appropriate variable in the structure. 
	*/

	for (int i = start; i < end; i++) 
		my_sum += (x[i] * y[i]);

	dot_product_result.sum = my_sum;
}

/*
The main program initializes data and calls the dotprd() function.
Finally, it prints the result.
*/

int main (int argc, char *argv[])
{
	double* a;
	double* b;
		
	/* Assign storage and initialize values */
	int len = LENGTH;
	a = (double*)malloc(len * sizeof(double));
	b = (double*)malloc(len * sizeof(double));
		
	for (int i = 0; i < len; i++)
		b[i] = a[i] = 1;

	dot_product_result.length = len; 
	dot_product_result.a = a; 
	dot_product_result.b = b; 
	dot_product_result.sum = 0;

	/* Perform the  dot_productuct */
	dot_product();

	/* Print result and release storage */ 
	printf ("Sum =  %f \n", dot_product_result.sum);
	free(a);
	free(b);
}