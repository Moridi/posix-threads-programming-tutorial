/******************************************************************************
* FILE: 2-arg-pass-basic.c
* DESCRIPTION:
*   A "hello world" Pthreads program.  Demonstrates thread creation and
*   termination.
* AUTHOR: Blaise Barney
* LAST REVISED: 2020-04-16 Mohammad Moridi
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include <pthread.h>

#define NUMBER_OF_THREADS 4
#define RESET_COLOR_INDEX 4

char COLOR_CODES[5][255] =
{
	"\033[1m\033[30m",		// Bold BLACK
	"\033[1m\033[31m",		// Bold RED
	"\033[1m\033[32m",		// Bold GREEN
	"\033[1m\033[34m",		// Bold BLUE
	"\x1B[0m"				// Reset
};

void* print_hello(void* tid)
{
	long thread_id = (long)tid;
	char result[255];
	
	sprintf(result, "%sHello World! It's me, thread #%ld!%s\n",
			COLOR_CODES[thread_id], thread_id,
			COLOR_CODES[RESET_COLOR_INDEX]);
	printf("%s", result);

	pthread_exit(NULL);
}

int main()
{
	pthread_t threads[NUMBER_OF_THREADS];
	int return_code;

	for(long tid = 0; tid < NUMBER_OF_THREADS; tid++)
	{
		printf("In main: creating thread %ld\n", tid);
		return_code = pthread_create(&threads[tid],
				NULL, print_hello, (void*)tid);

		if (return_code)
		{
			printf("ERROR; return code from pthread_create() is %d\n",
					return_code);
			exit(-1);
		}
	}

	/* Last thing that main() should do */
	pthread_exit(NULL);
}