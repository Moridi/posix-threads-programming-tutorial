/******************************************************************************
* FILE: 9-producer-consumer.c
* DESCRIPTION:
*   Simple producer consumer implementation.
* AUTHOR: Blaise Barney
* LAST REVISED: 2020-04-16 Mohammad Moridi
******************************************************************************/

#include "stdio.h"
#include "stdlib.h"
#include "unistd.h"

#include "semaphore.h"
#include "pthread.h"

#define BUFFER_SIZE 10

#define TRUE 1
#define RESET_COLOR_INDEX 4

char COLOR_CODES[5][255] =
{
	"\033[1m\033[30m",		// Bold BLACK
	"\033[1m\033[31m",		// Bold RED
	"\033[1m\033[32m",		// Bold GREEN
	"\033[1m\033[34m",		// Bold BLUE
	"\x1B[0m"				// Reset
};

sem_t empty_sem;
sem_t full_sem;

pthread_mutex_t mutex;
pthread_t producer_tid, consumer_tid;

int buffer[BUFFER_SIZE];
int counter;

void* producer(void* param)
{
    int item = 0;
    char result[255];

    while(TRUE)
    {
        sem_wait(&empty_sem);
        pthread_mutex_lock(&mutex);

        if (counter < BUFFER_SIZE)
        {
	        sprintf(result, "%sProducer produced data[%d]...%s\n",
                COLOR_CODES[1],
                counter,
                COLOR_CODES[RESET_COLOR_INDEX]);
        	printf("%s", result);

            buffer [counter++] = item++;
        }

        pthread_mutex_unlock(&mutex);
        sem_post(&full_sem);
    }
}

void* consumer(void* param)
{
    int item;
    char result[255];

    while(TRUE)
    {
        sleep(1);
        sem_wait(&full_sem);
        pthread_mutex_lock(&mutex);
        
        if (counter > 0)
        {
            sprintf(result, "%sConsumer consumed data[%d]...%s\n",
                COLOR_CODES[2],
                counter,
                COLOR_CODES[RESET_COLOR_INDEX]);
        	printf("%s", result);
            item = buffer[counter--];
        }

        pthread_mutex_unlock(&mutex);
        sem_post(&empty_sem);
    }
}

int main(void)
{
    pthread_mutex_init(&mutex, NULL);

    sem_init(&full_sem, 0, (unsigned int)0);
    sem_init(&empty_sem, 0, (unsigned int)BUFFER_SIZE);

    counter = 0;
    
    for(int j = 0; j < BUFFER_SIZE; j++)
        buffer[j] = 0;
    
    pthread_create(&producer_tid, NULL, producer, NULL);
    pthread_create(&consumer_tid, NULL, consumer, NULL);
    
    pthread_join(producer_tid,NULL);
    pthread_join(consumer_tid,NULL);
    
    return 0;
}